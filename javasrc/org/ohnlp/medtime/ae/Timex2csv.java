package org.ohnlp.medtime.ae;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import  org.apache.uima.UimaContext;
import  org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.ohnlp.medtime.type.MedTimex3;
import org.uimafit.util.JCasUtil;

import org.uimafit.component.JCasAnnotator_ImplBase;
import org.uimafit.component.initialize.ConfigurationParameterInitializer;
import org.uimafit.descriptor.ConfigurationParameter;
import org.uimafit.factory.ConfigurationParameterFactory;
import org.uimafit.factory.initializable.InitializableFactory;

import au.com.bytecode.opencsv.CSVWriter;

public class Timex2csv extends JCasAnnotator_ImplBase{

//	public static final String PARAM_DIR_OUT=ConfigurationParameterFactory.createConfigurationParameterName(Timex2csv.class,"dirOut");
//	@ConfigurationParameter(
//                            mandatory=true,
//			description = "This parameter providesthe output dir ")
//    private String dirOut;
	
    
    public static final String PARAM_DIR_OUT="dirOut";
	@ConfigurationParameter(
                            name=PARAM_DIR_OUT,mandatory=true,
                            description = "This parameter providesthe output dir ")
    private String dirOut;
    
	@Override
    public void initialize(UimaContext context) throws ResourceInitializationException{
        super.initialize(context);
        ConfigurationParameterInitializer.initialize(this, context);
    
    
    }
    @Override
	public void process(JCas jcas) throws AnalysisEngineProcessException {
		// TODO Auto-generated method stub	
    	JCas originalView;
		try {
			JCas x= jcas.getView("UriView");
			String originFileName=x.getSofaDataURI();
			String[] values=originFileName.split("\\/");
			String fileName=values[values.length-1];
            String reportName=fileName.substring(0,fileName.lastIndexOf("."));
			//String reportName=fileName.split("\\.")[0];
			JCas ptjcas = jcas.getView("_InitialView");
			AnnotationIndex<Annotation> medTimex=ptjcas.getAnnotationIndex(MedTimex3.type);
			Iterator<Annotation> it=medTimex.iterator();

			
			CSVWriter writer = new CSVWriter(new FileWriter(dirOut + reportName
					+ ".csv"));

			while (it.hasNext()) {
				MedTimex3 currAnno = (MedTimex3) it.next();
				int start = currAnno.getBegin();
				int end = currAnno.getEnd();
				//String annoText =currAnno.getCoveredText();
				int timexInstance = currAnno.getTimexInstance();
				String timexType = currAnno.getTimexType();
				String timexValue = currAnno.getTimexValue();
				String timexQuant = currAnno.getTimexQuant();
				String timexFreq = currAnno.getTimexFreq();
				String timexMod = currAnno.getTimexMod();
				String[] entires = new String[] { String.valueOf(start),
						String.valueOf(end), String.valueOf(timexInstance),
						timexType, timexValue, timexQuant, timexFreq, timexMod };
				writer.writeNext(entires);
			}
			writer.close();
   
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    	
		
	}

}
