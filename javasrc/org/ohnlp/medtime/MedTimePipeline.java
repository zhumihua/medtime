package org.ohnlp.medtime;
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import java.io.File;
import java.util.Date;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.resource.metadata.TypeSystemDescription;
import org.cleartk.util.cr.FilesCollectionReader;
import org.ohnlp.medtime.ae.Timex2csv;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.factory.TypeSystemDescriptionFactory;
import org.uimafit.pipeline.SimplePipeline;

/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class MedTimePipeline {

	public File inputDirectory;

	public static void main(String[] args) throws Exception {



	    CollectionReader collectionReader = FilesCollectionReader.getCollectionReader(args[0]);
	    
		AnalysisEngineDescription pipelineIncludingUmlsDictionaries = AnalysisEngineFactory.createAnalysisEngineDescription(
				"medtimedesc/aggregate_analysis_engine/MedTimeAggregateTAE");
        
        System.out.println("*******************"+args[1]+"*******************");
        
        AnalysisEngineDescription outputMedTimeAE = AnalysisEngineFactory.createPrimitiveDescription(
                                                                                             Timex2csv.class,
                                                                                             Timex2csv.PARAM_DIR_OUT,
                                                                                             args[1]);

		SimplePipeline.runPipeline(collectionReader, pipelineIncludingUmlsDictionaries,outputMedTimeAE);
	   //SimplePipeline.runPipeline(collectionReader, pipelineIncludingUmlsDictionaries);

	    System.out.println("Done at " + new Date());
	}


}
