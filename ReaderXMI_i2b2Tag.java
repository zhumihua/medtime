package org.apache.ctakes.clinicalpipeline;
/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.ctakes.assertion.util.AssertionConst;
import org.apache.ctakes.core.util.CtakesFileNamer;
import org.apache.ctakes.postagger.POSTagger;
import org.apache.ctakes.typesystem.type.syntax.BaseToken;
import org.apache.ctakes.typesystem.type.syntax.Lemma;
import org.apache.ctakes.typesystem.type.textspan.Sentence;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.text.AnnotationIndex;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.cleartk.util.cr.FilesCollectionReader;
import org.uimafit.component.xwriter.XWriter;
import org.uimafit.factory.AnalysisEngineFactory;
import org.uimafit.pipeline.SimplePipeline;
import org.uimafit.util.JCasUtil;
import org.uimafit.factory.JCasFactory;
import org.apache.ctakes.typesystem.type.i2b2.*;


/**
 * Run the plaintext clinical pipeline, using the dictionary of terms from UMLS.
 * Note you must have the UMLS password supplied in some way - see the 
 * User or Developer Guide for information on options for doing that.
 * Also note you need to have the UMLS dictionaries available (they 
 * are separate download from Apache cTAKES itself due to licensing).
 * 
 * Input and output directory names are taken from {@link AssertionConst} 
 * 
 */
public class ReaderXMI_i2b2Tag {


	private JCas xmiFile;
	
	
	
	
	public static void printSent(String sent){
		System.out.println("`````````````");
		System.out.println(sent);
		System.out.println("`````````````");
	}
	
	public static ArrayList<File> listFiles(String dirName){
		File folder=new File(dirName);
		File[] listFiles=folder.listFiles();
		
		ArrayList<File> rets=new ArrayList<File>();
		for(int i=0;i<listFiles.length;i++){
			if(listFiles[i].isFile()&&listFiles[i].getName().endsWith(".xmi")){
				rets.add(listFiles[i]);
			}	
			}
		return rets;
	}
	

	//posTag
	public static void xmi_tag_verb_sec(String xmiName,String dirOut) throws Exception{
		
		
		JCas xmiFile=JCasFactory.createJCas();
		JCasFactory.loadJCas(xmiFile, xmiName);
		JCas x= xmiFile.getView("UriView");
		String originFileName=x.getSofaDataURI();
		String[] values=originFileName.split("\\/");
		String fileName=values[values.length-1];
		PrintWriter writer=new PrintWriter(new File(dirOut+fileName+".txt"));
		
		JCas ptjcas=xmiFile.getView("plaintext");
		String originalText=ptjcas.getDocumentText();
		
		
		Map<GoldTag,Collection<Sentence>> map_i2b2Annot_sent=JCasUtil.indexCovering(ptjcas, GoldTag.class, Sentence.class);
		Map<Sentence,Collection<BaseToken>> map_sent_token=JCasUtil.indexCovered(ptjcas, Sentence.class, BaseToken.class);
		
		for(GoldTag aTag:map_i2b2Annot_sent.keySet()){
			ArrayList<BaseToken> verbsPOS=new ArrayList<BaseToken>();
			Collection<Sentence> sents=map_i2b2Annot_sent.get(aTag);
			if(sents==null||sents.size()>1){
				System.out.println("sents size bigger than 1 or no sents");
				System.exit(0);
			}
		
			for(Sentence sent:sents){
				for(BaseToken token:map_sent_token.get(sent)){
					String pos=token.getPartOfSpeech();
					if(pos.startsWith("V")||pos.startsWith("M")){
						verbsPOS.add(token);
					}
				}
				
			}	
			
			
			int start=aTag.getBegin();
			int end=aTag.getEnd();
			String annotText=originalText.substring(start,end);
			String secName=aTag.getSectName();
			String indicatorName="";
			if(aTag instanceof GoldMedication){
				GoldMedication medication=(GoldMedication)aTag;	
				indicatorName=medication.getType1();

			}else if(aTag instanceof GoldDisease){
				GoldDisease disease=(GoldDisease)aTag;
				indicatorName=disease.getIndicator();
			}else{
				writer.println(aTag.getClass().getName());
				System.exit(0);
			}
			String strPos="";
			for(BaseToken pos:verbsPOS){
				strPos=strPos+"\t"+pos.getPartOfSpeech();
			}
			
			//writer.println(aTag.getTime_value()+"\t"+annotText+"\t"+aTag.getSectName()+"\t"+indicatorName+strPos);
			writer.println(aTag.getTime_value()+"\t"+aTag.getSectName()+"\t"+indicatorName+strPos);
			
		}
		writer.close();
		
	}

	
	public static void printPOSDS(String inDir,String outDir) throws Exception{
		ArrayList<File> inFiles=listFiles(inDir);
		
		for(File fxmi:inFiles){
			String aXmi=fxmi.getAbsolutePath();
			
			xmi_tag_verb_sec(aXmi,outDir);

		}
		
	}
	
	public static void main(String[] args) throws Exception {
		
		printPOSDS(args[0],args[1]);
		
		//===============================================================

//		//if(args==null||args.length)
//		
//		JCas xmiFile=JCasFactory.createJCas();
//		JCasFactory.loadJCas(xmiFile, "data/output/2.xmi");
//		JCas ptjcas=xmiFile.getView("plaintext");
//		
//		Map<GoldTag,Collection<Sentence>> map_i2b2Annot_sent=JCasUtil.indexCovering(ptjcas, GoldTag.class, Sentence.class);
//		for(GoldTag aTag:map_i2b2Annot_sent.keySet()){
//			Collection<Sentence> sents=map_i2b2Annot_sent.get(aTag);
//			if(sents==null||sents.size()>1){
//				System.out.println("sents size bigger than 1 or no sents");
//				System.exit(0);
//			}
//			String originalText=ptjcas.getDocumentText();
////			printAnnotSent(aTag,sents,originalText);
//		}
		
		//===============================================================
//		Map<Sentence,Collection<tag>> map_sent_i2b2Annot=JCasUtil.indexCovered(ptjcas, Sentence.class, tag.class);
//		for(Sentence sent:map_sent_i2b2Annot.keySet()){
//			Collection<tag> tags=map_sent_i2b2Annot.get(sent);
//			if(tags!=null||tags.size()>0){
//				for(tag aTag:tags){
//					JCasUtil.
//				}
//			}
//		}
		
//		String text=xmiFile.getDocumentText();
//		AnnotationIndex<Annotation> sentAnnot=xmiFile.getAnnotationIndex(Sentence.type);
//		Iterator<Annotation> it=sentAnnot.iterator();
//		
//		while(it.hasNext()){
//			Annotation currAnno=it.next();
//			int start=currAnno.getBegin();
//			int end=currAnno.getEnd();
//			String sent=text.substring(start,end);
//			printSent(sent);
//		}
		
	}


}
